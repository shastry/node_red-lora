'''
Copyright 2019 Javier Errea, Ivan Eroshkin and Dhiaeddine Alioui from EURECOM

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''

from flask_sqlalchemy import SQLAlchemy
from flask import Flask
from flask_mqtt import Mqtt
from flask_socketio import SocketIO
#from flask_bootstrap import Bootstrap

app = Flask(__name__) # Creates instance of web application
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///lora.db'
app.config['SECRET'] = ''
app.config['TEMPLATES_AUTO_RELOAD'] = True
# First broker -- App server side
#app.config['MQTT_BROKER_URL'] = '172.16.70.132'
app.config['MQTT_BROKER_URL'] = 'localhost'
app.config['MQTT_BROKER_PORT'] = 2550
app.config['MQTT_USERNAME'] = ''
app.config['MQTT_PASSWORD'] = ''
app.config['MQTT_KEEPALIVE'] = 5
app.config['MQTT_TLS_ENABLED'] = False
mqtt = Mqtt(app)
# Second broker -- Flask Server side
app.config['MQTT_BROKER_URL'] = 'localhost'
app.config['MQTT_BROKER_PORT'] = 2551
app.config['MQTT_USERNAME'] = ''
app.config['MQTT_PASSWORD'] = ''
app.config['MQTT_KEEPALIVE'] = 5
app.config['MQTT_TLS_ENABLED'] = False
mqtt1 = Mqtt(app)

socketio = SocketIO(app)
#bootstrap = Bootstrap(app)
db = SQLAlchemy(app)
db.init_app(app)


with app.app_context():
    from . import routes
    from . import models
    db.create_all()
