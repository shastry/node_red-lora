'''
Copyright 2019 Javier Errea, Ivan Eroshkin and Dhiaeddine Alioui from EURECOM

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''

from app.models import Users, Applications, Devices
from app import app, db, mqtt, mqtt1, socketio
from flask import json, request#, redirect, url_for, jsonify
import threading, time, requests
import secrets
from datetime import datetime as dt
from collections import OrderedDict

#from flask_login import login_user

__jwt = ""
__app_id = 0
#__lora_server = "http://172.16.70.132:8080" # We should have the ip of the app server
__lora_server = "http://localhost:8080" # We should have the ip of the app server
__abp = "e8968223-97aa-4f66-90b9-04c2ad120248" # They should be pre set at the app server
__otaa = "8cace802-6e34-4bce-8076-8460717d08d6" # They should be pre set at the app server
__serv_prof_id = "a4f2a044-09cd-4cd2-8106-d1986019835f"
#D145CEDD26A44586
# Remember that we need to obtain first the jwt token in order to be able to query the appserver (internal services --> log in user)
services = [

        {
            '1. id': 1,
            '2. title': 'Authentication',
            '3. method': 'POST',
            '4. url': 'http://flask_server_address:5000/services/auth',
            '5. description': 'This must be the first service to be executed. User will be granted with a secret key, needed for every other service request',
            '6. usage': {
                        "username":"string",
                        "password":"string"
                     }
        },
        {
            '1. id': 2,
            '2. title': 'Create an application',
            '3. method': 'POST',
            '4. url': 'http://flask_server_address:5000/services/createapp',
            '5. description': u'This service will creates a new application.',
            '6. usage': {
                        "secret key":"string",
                        "description":"string",
                        "application name":"string"
                     }
        },
        {
            '1. id': 3,
            '2. title': 'Delete application',
            '3. method': 'DELETE',
            '4. url': 'http://flask_server_address:5000/services/deleteapp',
            '5. description': 'This service will delete an application based on the app id given by the user.',
            '6. usage': {
                        "secret key":"string",
                        "application id":"string"
                     }
        },
        {
            '1. id': 4,
            '2. title': 'Update application',
            '3. method': 'PUT',
            '4. url': 'http://flask_server_address:5000/services/updateapp',
            '5. description': 'This service will update the parameters of a given application.',
            '6. usage': {
                        "secret key":"string",
                        "description":"string",
                        "application name":"string"
                     }
        },
        {
            '1. id': 5,
            '2. title': 'Create device',
            '3. method': 'POST',
            '4. url': 'http://flask_server_address:5000/services/createdevice',
            '5. description': 'This service will create a new device associated to a given application.',
            '6. usage': {
                        "secret key":"string",
                        "authentication method":"otaa/abp",
                        "description":"string",
                        "device eui":"string",
                        "device name":"string"
                     }
        },
        {
            '1. id': 6,
            '2. title': 'Update device',
            '3. method': 'PUT',
            '4. url': 'http://flask_server_address:5000/services/updatedevice',
            '5. description': 'This service will update the parameters of a given dev EUI.',
            '6. usage': {
                         "secret key":"string",
                         "authentication method":"otaa/abp",
                         "application name":"string",
                         "description":"string",
                         "device eui":"string",
                         "device name":"string"
                     }
        },
        {
            '1. id': 7,
            '2. title': 'Delete device',
            '3. method': 'DELETE',
            '4. url': 'http://flask_server_address:5000/services/deletedevice',
            '5. description': 'This service will delete devices according to the provided EUI.',
            '6. usage': {
                        "secret key":"string",
                        "device eui":"string"
                     }
        },
        {
            '1. id': 8,
            '2. title': 'Configure ABP authentication method',
            '3. method': 'POST',
            '4. url': 'http://flask_server_address:5000/services/configabp',
            '5. description': 'This service will configure (and updates as well) and activate device keys for ABP authentication method.',
            '6. usage': {
                         "secret key":"string",
                         "app session key":"string",
                         "device address":"string",
                         "device eui":"string",
                         "network session key":"string"
                     }
        },
        {
            '1. id': 9,
            '2. title': 'Configure OTAA authentication method',
            '3. method': 'POST',
            '4. url': 'http://flask_server_address:5000/services/configotaa',
            '5. description': 'This service will configure device keys for OTAA authentication method credentials. They will be activated once the device has sent data.',
            '6. usage': {
                         "secret key":"string",
                         "application key":"string",
                         "device eui":"string"
                     }
        },
        {
            '1. id': 10,
            '2. title': 'Update OTAA authentication method',
            '3. method': 'PUT',
            '4. url': 'http://flask_server_address:5000/services/updateotaa',
            '5. description': 'This service will update device keys for OTAA authentication method credentials',
            '6. usage': {
                         "secret key":"string",
                         "application key":"string",
                         "device eui":"string"
                     }
        },
        {
            '1. id': 11,
            '2. title': 'Get application session key',
            '3. method': 'POST',
            '4. url': 'http://flask_server_address:5000/services/getappkey',
            '5. description': 'This service will get the Application Session Key of a given device EUI, so that the user can decrypt the data',
            '6. usage': {
                         "secret key":"string",
                         "device eui":"string"
                     }
        }
    ]


def verify(secret):
    user = Users.query.filter_by(secret_key=secret).first()
    if(user):
        return str(user.username)
    else:
        return False


############################
###### MQTT functions ######
############################
@mqtt.on_message()
def handle_mqtt_message(client, userdata, message):

    data = dict(
        topic=message.topic,
        payload=message.payload.decode()
    )
    # Get app_id
    string_data = data['payload']
    json_data = json.loads(string_data)
    local_app_id = json_data['devEUI']
    
    #On receive message make http request similar to generate message function
    #eui_new = json_data['devEUI']
    #print("DEBUG NEW EUI"+str(eui_new))
    
    # Publish message to flask side mqtt broker
    mqtt1.publish(local_app_id, data['payload'])
    socketio.emit('mqtt_message', data=data)
    print(string_data)


@socketio.on('subscribe')
def handle_subscribe(topic):
    mqtt.subscribe(topic)
    thread = threading.Thread(target=handle_mqtt_message)
    thread.start()



###########################
###### JWT generator ######
###########################
@app.before_first_request
def get_token():
    def new_jwt():
        while True:
            json_data = {
                "password": "admin",
                "username": "admin"
            }
            headers = {'content-type': 'application/json'}
            r = requests.post(__lora_server+'/api/internal/login', headers=headers, json=json_data)
            global __jwt
            __jwt = json.loads(r.content)['jwt']
            time.sleep(900)

    thread = threading.Thread(target=new_jwt)
    thread.start()


# Automatically subscribe the flask server to all topics on load
@app.before_first_request
def subscribetoalltopics():
    # Get list of app ids from the database and subscribe to all topics
    apps = Applications.query.all()
    for app in apps:
        topic = "application/" + str(app.app_id) + "/device/+/rx"
        print(topic)
        handle_subscribe(topic)
        print("Subscribed")


#######################
###### Discovery ######
#######################
@app.route("/services/discoverapi", methods=['GET'])
def get_services():
    sort_order = ['1. id', '2. title', '3. method', '4. url', '5. description', '6. usage']
    services_ordered = [OrderedDict(sorted(serv.items(), key=lambda item: sort_order.index(item[0])))
                        for serv in services]
    print(json.dumps(services_ordered, indent=4, separators=(',', ': ')))
    return str(json.dumps(services_ordered, indent=4, separators=(',', ': '))) #jsonify({'services': services})


#################################
###### User authentication ######
#################################
@app.route("/services/auth", methods=['GET', 'POST'])
def user_auth():
    user_data = request.json
    user = Users.query.filter_by(username=user_data['username']).first()
    if user and user_data['password'] == user.password:
        print("Success")
        token = secrets.token_urlsafe(10)
        json_data = {
            "secret key": token
        }
        user.secret_key = token
        db.session.commit()
        return str(json_data['secret key'])
    else:
        return 'Authentication Failed'


####################################
####### Application services #######
####################################
@app.route("/services/createapp", methods=['POST'])
def create_app():
    #Parameters needed from the user: app name and organization and description and service profile id
    #Map all this info in database (missing organization name and service profile name from user and application name)
    user_data = request.json
    user = verify(user_data['secret key'])
    print(user)
    if(user):
        headers = {
            "content-type": "application/json",
            "Authorization": "Bearer "+__jwt
        }
        json_data = {
            "application": {
                "description": user_data['description'],
                "id": "1",
                "name": user_data['application name'],
                "organizationID": "1",
                "payloadCodec": "",
                "payloadDecoderScript": "",
                "payloadEncoderScript": "",
                "serviceProfileID": __serv_prof_id
            }
        }
        r = requests.post(__lora_server+'/api/applications', headers=headers, json=json_data)
        #global app_id
        try:
            l_app_id = (r.json())['id']
        except:
            return(str(r.content))
        newApp = Applications(app_name=user_data['application name'], app_id=l_app_id, owner=user,
                              creation_date=dt.now())
        db.session.add(newApp)
        db.session.commit()
        #db.session.close()
        topic = "application/" + str(l_app_id) + "/device/+/rx"
        handle_subscribe(topic)
        return l_app_id
    else:
        return 'Authentication failed'


@app.route("/services/deleteapp", methods=['DELETE'])
def delete_app():
    # Application id given by user. Or application name, which will be parsed with the id of the app in the data base
    user_data = request.json
    user = verify(user_data['secret key'])
    if user == False:
        return "Authentication failed !"
    app = Applications.query.filter_by(app_id=user_data['application id']).first()
    if app == None:
        return "Application doesn't exist"
    if app.owner != user:
        return "Permission denied !!"

    # Delete all the devices of this application from the flask server database
    devs = Devices.query.filter_by(app_id=app.app_id).all()
    for dev in devs:
        db.session.delete(dev)

    db.session.delete(app)
    db.session.commit()

    headers = {
        "content-type": "application/json",
        "Authorization": "Bearer " + __jwt
    }

    r = requests.delete(__lora_server + '/api/applications/' + user_data['application id'], headers=headers)
    if str(r.content) == "b'{}'":
        return "Application deleted !"
    return str(r.content)


@app.route("/services/updateapp", methods=['PUT'])
def update_app():
    user_data = request.json
    user = verify(user_data['secret key'])
    if user == False:
        return "Authentication failed !"
    app = Applications.query.filter_by(app_id=user_data['application id']).first()
    if app == None:
        return "Application doesn't exist !"
    if app.owner != user:
        return "Permission denied !!"

    # Application id given by user. Or application name, which will be parsed with the id of the app in the data base
    headers = {
        "content-type": "application/json",
        "Authorization": "Bearer " + __jwt
    }
    # user will give the name of the app which will be translated with its ID
    json_data = {
        "application": {
            "description": user_data['description'],
            "id": user_data['application id'],
            "name": user_data['application name'],
            "organizationID": "1",
            "payloadCodec": "",
            "payloadDecoderScript": "",
            "payloadEncoderScript": "",
            "serviceProfileID": __serv_prof_id
        }
    }
    r = requests.put(__lora_server + '/api/applications/' + user_data['application id'], headers=headers,
                     json=json_data)
    if str(r.content) == "b'{}'":
        app.app_name = user_data['application name']
        db.session.commit()
        return "Update successful !"
    return str(r.content)


####################################
######### Devices Services #########
####################################
@app.route("/services/createdevice", methods=['POST'])
def create_device():

    user_data = request.json
    user = verify(user_data['secret key'])
    if(user==False):
        return "Authentication failed"
    app = Applications.query.filter_by(app_id=user_data['application id']).first()
    if(app==None):
        return "Application doesnt exist"
    if(app.owner != user):
        return "Permission denied"
    auth_meth = user_data['authentication method']

    # Taken from DB
    if(auth_meth == "otaa"):
        dev_profile=__otaa # OTAA both will be in database (list from lora server)
    elif(auth_meth == "abp"):
        dev_profile = __abp  # ABP both will be in database (list from lora server)
    else:
        return "Incorrect authentication method provided"
    json_data = {
        "device": {
            "applicationID": user_data['application id'],
            "description": user_data['description'],
            "devEUI": user_data['device eui'],
            "deviceProfileID": dev_profile,
            "name": user_data['device name'],
            "ReferenceAltitude": "0", # By default
            "skipFCntCheck": True  # By default
        }
    }
    headers = {
        "content-type": "application/json",
        "Authorization": "Bearer "+__jwt
    }
    r = requests.post(__lora_server+'/api/devices', headers=headers, json=json_data)
    if str(r.content) == "b'{}'":
        newDev = Devices(dev_eui=user_data['device eui'],
                         auth_method=dev_profile, app_id=user_data['application id'], register_date=dt.now())
        db.session.add(newDev)
        db.session.commit()
        #db.session.close()
        return "Device created successfully"
    return str(r.content)


@app.route("/services/updatedevice", methods=['PUT'])
def update_device():
    user_data = request.json
    user = verify(user_data['secret key'])
    if (user == False):
        return "Authentication failed"
    dev = Devices.query.filter_by(dev_eui=user_data['device eui']).first()

    if (dev == None):
        return "Devices doesnt exist"
    app = Applications.query.filter_by(app_id=dev.app_id).first()
    if (app.owner != user):
        return "Permission denied"

    auth_meth = user_data['authentication method']

    # Taken from DB
    if (auth_meth == "otaa"):
        dev_profile = __otaa  # OTAA both will be in database (list from lora server)
    elif (auth_meth == "abp"):
        dev_profile = __abp  # ABP both will be in database (list from lora server)
    else:
        return "Incorrect authentication method provided"

    json_data = {
        "device": {
            "applicationID": app.app_id,
            "description": user_data['description'],
            "devEUI": "",
            "deviceProfileID": dev_profile,
            "name": user_data['device name'],
            "ReferenceAltitude": 0,
            "skipFCntCheck": False  # user_data['counter enabled']
        }
    }
    headers = {
        "content-type": "application/json",
        "Authorization": "Bearer " + __jwt
    }
    r = requests.put(__lora_server + '/api/devices/' + user_data['device eui'], headers=headers, json=json_data)
    if str(r.content) == "b'{}'":
        dev.auth_method = auth_meth
        db.session.commit()
        return "Update successful "

    return str(r.content)


@app.route("/services/deletedevice", methods=['DELETE'])
def delete_device():
    user_data = request.json
    user = verify(user_data['secret key'])
    if (user == False):
        return "Authentication failed"
    dev = Devices.query.filter_by(dev_eui=user_data['device eui']).first()

    if (dev == None):
        return "Devices doesnt exist"

    app = Applications.query.filter_by(app_id=dev.app_id).first()
    if (app.owner != user):
        return "Permission denied"
    db.session.delete(dev)
    db.session.commit()
    headers = {
        "content-type": "application/json",
        "Authorization": "Bearer " + __jwt
    }
    r = requests.delete(__lora_server + '/api/devices/' + user_data['device eui'], headers=headers)
    if str(r.content) == "b'{}'":
        return "Device deleted !"
    return str(r.content)


#####################################
######### Configure devices #########
#####################################
# also used for update
@app.route("/services/configabp", methods=['POST'])
def config_abp():
    user_data = request.json
    user = verify(user_data['secret key'])
    if (user == False):
        return "Authentication failed"
    dev = Devices.query.filter_by(dev_eui=user_data['device eui']).first()

    if (dev == None):
        return "Devices doesnt exist"
    app = Applications.query.filter_by(app_id=dev.app_id).first()
    if (app.owner != user):
        return "Permission denied"
    json_data = {
        "deviceActivation": {
            "aFCntDown": 0,
            "appSKey": user_data['app session key'],
            "devAddr": user_data['device address'],
            "devEUI": user_data['device eui'],
            "fCntUp": 0,
            "fNwkSIntKey": user_data['network session key'],
            "nFCntDown": 0,
            "nwkSEncKey": user_data['network session key'],
            "sNwkSIntKey": user_data['network session key']
        }
    }
    headers = {
        "content-type": "application/json",
        "Authorization": "Bearer "+__jwt
    }
    r = requests.post(__lora_server+'/api/devices/'+user_data['device eui']+'/activate', headers=headers, json=json_data)

    if str(r.content) == "b'{}'":
        dev.app_sess_key = user_data['app session key']
        db.session.commit()
        return "Activation successful"
    return str(r.content)


@app.route("/services/configotaa", methods=['POST'])
def config_otaa():

    user_data = request.json
    user = verify(user_data['secret key'])
    if (user == False):
        return "Authentication failed"
    dev = Devices.query.filter_by(dev_eui=user_data['device eui']).first()

    if(dev==None):
        return "Devices doesnt exist"
    app = Applications.query.filter_by(app_id=dev.app_id).first()
    if(app.owner != user):
        return "Permission denied"
    json_data = {
        "deviceKeys": {
            "appKey": user_data['application key'],
            "devEUI": user_data['device eui'],
            "nwkKey": user_data['application key']
        }
    }
    headers = {
        "content-type": "application/json",
        "Authorization": "Bearer "+__jwt
    }
    r = requests.post(__lora_server+'/api/devices/'+user_data['device eui']+'/keys', headers=headers, json=json_data)
    if str(r.content) == "b'{}'":
        return "OTAA Configuration successful"
    return str(r.content)


@app.route("/services/updateotaa", methods=['PUT'])
def update_otaa():
    user_data = request.json
    user = verify(user_data['secret key'])
    if (user == False):
        return "Authentication failed"
    dev = Devices.query.filter_by(dev_eui=user_data['device eui']).first()

    if (dev == None):
        return "Devices doesnt exist"
    app = Applications.query.filter_by(app_id=dev.app_id).first()
    if (app.owner != user):
        return "Permission denied"
    json_data = {
        "deviceKeys": {
            "appKey": user_data['application key'],
            "devEUI": user_data['device eui'],
            "nwkKey": user_data['application key']  # This one is taken as appKey
        }
    }
    headers = {
        "content-type": "application/json",
        "Authorization": "Bearer " + __jwt
    }
    r = requests.put(__lora_server + '/api/devices/' + user_data['device eui'] + '/keys', headers=headers,
                     json=json_data)
    if str(r.content) == "b'{}'":
        return "OTAA Configuration update successful"

    return str(r.content)


#############################################################
###### Get application session key for data decription ######
#############################################################
@app.route("/services/getappkey", methods=['POST'])
def getappsesskey():
    user_data = request.json
    user = verify(user_data['secret key'])
    if (user == False):
        return "Authentication failed"
    dev = Devices.query.filter_by(dev_eui=user_data['device eui']).first()

    if (dev == None):
        return "Devices doesnt exist"
    app = Applications.query.filter_by(app_id=dev.app_id).first()
    if (app.owner != user):
        return "Permission denied"

    headers = {
        "content-type": "application/json",
        "Authorization": "Bearer " + __jwt
    }
    if(dev.app_sess_key==None):
        r_app_sess_key = requests.get(__lora_server + '/api/devices/' + user_data['device eui'] + '/activation',
                                    headers=headers)
        try:
            devAct = (r_app_sess_key.json())['deviceActivation']
        except:
            return "Device not activated"

        app_sess_key = devAct['appSKey']
        dev.app_sess_key = app_sess_key
        db.session.commit()

    return str(dev.app_sess_key)

##########################
###### Home service ######
##########################TESTINGS
@app.route("/home")
def hello():
    return str("ree")
import os

@app.route("/test122", methods=['GET'])
def hello2():
    print(request.args.get('query'))
    return "HI I'M WORKING"


from random import randint
import json
###########################
######  CONF FILE #########
###########################
@app.route("/Newtest", methods=['GET'])
def nodered_test():
    print("Hello this is the flask printing")
    query=request.args.get('query')
    #taking EUI value from the query string
    ind1=query.find('device eui')
    #print(test_data[ind1+14:ind1+30])
    idd= "Your id is "+ str(query[ind1+13:ind1+29])    
    modify_yaml(query,str(query[ind1+13:ind1+29]))
    #modify_yaml()
    return idd
#testing to modify the yaml file
def modify_yaml(query,idd):
    print("modifying yaml")
    #test_data="configuration: {'username': 'javier','password': '12345','applications':{'application1':{'description application': 'New_App','application name': 'IOTAAA','devices':{'device1':{'authentication method': 'otaa','description device': 'brand new device','device eui': '6102040405067677','device name': 'device_otaa','configuration':{'application key': '53acaeaa3438c7ff6726876dae220032','device eui': '610204040507777','network key': 'a65fc1832975b4c4fd9e162fb4debc49'}}}}}}"
    try:
        file_yaml= open(os.getcwd() + '/config_template.yaml','w+')
    except IOError:
        print("Could not open file! Please close Excel!")
    #file_yaml.write(test_data)
    file_yaml.write(query)
    print(query)
    file_yaml.close()
    #execute the script.py python script
    os.system('python3 script.py')
    generate_random_messages(idd)
    print("work done")
    return "WORK DONE"
import requests 
import time
def generate_random_messages(idd):
    i=0
    while(i<4):
        #GET EUI from device ex:6102040405060501
        eui=str(idd)
        #api-endpoint 
        URL = "http://localhost:1880/"+eui  
        print(URL)
        #message Get it from api
        message="Hi you "+str(i)
        #defining a params dict for the parameters to be sent to the API 
        PARAMS = {'message':message}  
        #sending get request and saving the response as response object 
        r = requests.get(url = URL, params = PARAMS) 
        i=i+1
        time.sleep(1)
    return
