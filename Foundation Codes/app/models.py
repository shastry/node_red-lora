'''
Copyright 2019 Javier Errea, Ivan Eroshkin and Dhiaeddine Alioui from EURECOM

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''

from app import db


class Users(db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    password = db.Column(db.String, nullable=False)
    secret_key = db.Column(db.String)
    # applications = db.relationship('Applications', backref='username', lazy=True)
    # devices = db.relationship('Devices', backref='owner', lazy=True)

    def __repr__(self):
        return f"User('{self.username}', '{self.password}')"


class Applications(db.Model):
    """"""
    __tablename__ = "apps"

    id = db.Column(db.Integer, primary_key=True)
    app_name = db.Column(db.String)
    app_id = db.Column(db.Integer)
    owner = db.Column(db.String, db.ForeignKey('users.username'), nullable=False)
    creation_date = db.Column(db.DateTime, nullable=False)

    def __repr__(self):
        return f"Application('{self.owner}', '{self.app_name}', '{self.app_id}, '{self.creation_date})"


class Devices(db.Model):
    """"""
    __tablename__ = "devices"

    id = db.Column(db.Integer, primary_key=True)
    dev_eui = db.Column(db.String)
    auth_method = db.Column(db.String)
    app_id = db.Column(db.Integer, db.ForeignKey('apps.app_id'), nullable=False)
    app_sess_key = db.Column(db.String)
    register_date = db.Column(db.DateTime, nullable=False)

    def __repr__(self):
        return f"Devices('{self.dev_eui}', '{self.auth_method}', '{self.app_id}', '{self.register_date}')"