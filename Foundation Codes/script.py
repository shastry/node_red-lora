'''
Copyright 2019 Javier Errea, Ivan Eroshkin and Dhiaeddine Alioui from EURECOM

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''

import requests
import yaml
import os

__flask_server = "http://localhost:5000"
# Put location of the config_template.yaml
file = open(os.getcwd() + '/config_template.yaml', 'r')
doc = yaml.load(file)
print(doc)

headers = {
	"content-type": "application/json",
}

num_of_app = len(doc['configuration']['applications'])

data = {
	"username": str(doc['configuration']['username']),
	"password": str(doc['configuration']['password'])
}
r = requests.post(__flask_server+'/services/auth', headers=headers, json=data)
print(data)
# Obtain secret key. It will be attached to all messages here later.
print("----------- Secret=" + str(r.text)) 
secret=r.text
print(num_of_app)
# Create new application
for app in range(1, num_of_app+1):

	data_app = {
		"secret key": secret,
		"description": str(doc['configuration']['applications']['application'+ str(app)]['description application']),
		"application name": str(doc['configuration']['applications']['application'+ str(app)]['application name'])
	}
	print(data_app)
	#print("t1")
	r_app = requests.post(__flask_server+'/services/createapp', headers=headers, json=data_app)
	#print("test1")	
	print(r_app)	
	# Obtain app id
	print("----------- Application created with id=" + str(r_app.text))
	app_id=r_app.text
	print(app_id)
	num_of_devices = len(doc['configuration']['applications']['application'+ str(app)]['devices'])
	for dev in range(1, num_of_devices+1):
		# Create new device and attach it to the created authentication
		data_dev = {
			"secret key": secret,
			"authentication method": str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['authentication method']),
			"application id": app_id,
			"description": str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['description device']),
			"device eui": str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['device eui']),
			"device name": str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['device name'])
		}
		r_dev = requests.post(__flask_server+'/services/createdevice', headers=headers, json=data_dev)
		print("----------- Device created" + str(r_dev.content))
		if str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['authentication method']) == "otaa":
			data_config = {
				"secret key": secret,
				"application key": str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['configuration']['application key']),
				"device eui": str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['configuration']['device eui'])
				# For LoRaWAN 1.1.0 uncomment it
				#		 "network key": str(doc['network key'])
				
			}
			r_conf = requests.post(__flask_server+'/services/configotaa', headers=headers, json=data_config)
			print("----------- Configuration of device completed" + str(r_conf.content)) 
		elif str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['authentication method']) == "abp":
			data_config = {
				"secret key": secret,
				"app session key": str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['configuration']['app session key']),
				"device address": str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['configuration']['device address']),
				"device eui": str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['configuration']['device eui']),
				"network session key": str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['configuration']['network key']) # And delete this
				# For LoRaWAN 1.1.0 uncomment it
				#		"forwarding network session key": str(doc['forwarding network session key']),
				#		"network session encryption key": str(doc['network session encryption key']) 
				#		"network session integrity key": str(doc['network session integrity key'])
			}
			r_conf = requests.post(__flask_server+'/services/configabp', headers=headers, json=data_config)
			print("----------- Configuration of device completed" + str(r_conf.content)) 

print("Ce fini. Goodbye!")
