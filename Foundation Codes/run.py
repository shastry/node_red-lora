'''
Copyright 2019 Javier Errea, Ivan Eroshkin and Dhiaeddine Alioui from EURECOM

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''

from app import app, socketio
import threading, time, requests

def start_runner():
    def start_loop():
        not_started = True
        while not_started:
            #print('In start loop')
            try:
                r = requests.get('http://127.0.0.1:5000/home')
                if r.status_code == 200:
                    #print('Server started, quiting start_loop')
                    not_started = False
                #print(r.status_code)
            except:
                #print('Server not yet started')
                pass
            time.sleep(2)

    #print('Started runner')
    thread = threading.Thread(target=start_loop)
    thread.start()

if __name__ == '__main__':
    start_runner()
    socketio.run(app, host='127.0.0.1', port=5000, use_reloader=True, debug=True)
