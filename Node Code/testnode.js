module.exports = function(RED) {
    function create_yaml(config) {
        RED.nodes.createNode(this,config);
        var node = this;
        node.on('input', function(msg) {
            if(config.authentication_method=="otaa"){
                modified="'application key': "+config.application_key+",'device eui': "+config.device_eui+",'network key': "+config.network_key
            }
            else
                modified="'app session key': "+config.app_session_key+",'device_address': "+config.device_address+",'device eui': "+config.device_eui+",'network key': "+config.network_key;
        msg.payload = "configuration: {'username': "+config.username+",'password': "+config.password+",'applications':{'application1':{'description application': "+config.application_description+",'application name': "+config.application_name+",'devices':{'device1':{'authentication method': "+config.authentication_method+",'description device': "+config.description_device+",'device eui': "+config.device_eui+",'device name': "+config.device_name+",'configuration':{"+modified+"}}}}}}";
        node.send(msg);
        });
    }
    RED.nodes.registerType("test_node_lora",create_yaml);
}