# LORA
**What is Lora?**

LoRa is a low-power wide-area network technology. It is based on spread spectrum modulation techniques derived from chirp spread spectrum technology.

**Why Lora?**

•	It uses 868 MHz/ 915 MHz ISM bands which is available world wide.

•	It has very wide coverage range about 5 km in urban areas and 15 km in suburban areas.

•	It consumes less power and hence battery will last for longer duration.

•	Single LoRa Gateway device is designed to take care of 1000s of end devices or nodes.

•	It is easy to deploy due to its simple architecture as shown in the figure-1.

•	It uses Adaptive Data Rate technique to vary output data rate/Rf output of end devices. This helps in maximizing battery life as well as overall capacity of the LoRaWAN network. The data rate can be varied from 0.3 kbps to 27 Kbps for 125 KHz bandwidth.

•	It is widely used for M2M/IoT applications.

**How?**

Chirpstack.io provides detailed information on how to install and get started with own lora server. Currently LORA servers are best suited for Linux machines and if were to opearate on windows, a Virtual Machine or other Linux OS mechanisms should be present. The following are few pre requisites for LORA to be installed:
1. ***MQTT Broker*** - MQTT is a machine-to-machine (M2M)/"Internet of Things" connectivity protocol. It was designed as an extremely lightweight publish/subscribe messaging transport. It is useful for connections with remote locations where a small code footprint is required and/or network bandwidth is at a premium. In the project, ChirpStack makes use of MQTT for publishing and receiving application payloads. We use "Mosquitto" as the MQTT broker, as it is simple and flexible. MQTT is used by ChirpStack Gateway Bridge, ChirpStack Network Server, and ChirpStack Application Server.
2. ***PostgreSQL database*** - PostgreSQL, also known as Postgres, is a free and open-source relational database management system emphasizing extensibility and technical standards compliance. It is designed to handle a range of workloads, from single machines to data warehouses or Web services with many concurrent users. The ChirpStack components are using PostgreSQL for persistent data-storage. As this project does not currently deal much with such data storages, this is just a pre requisite to be completed.
3. ***Redis Database*** - Redis is an in-memory data structure project implementing a distributed, in-memory key-value database with optional durability.  Redis supports different kinds of abstract data structures, such as strings, lists, maps, sets, sorted sets, HyperLogLogs, bitmaps, streams, and spatial indexes. The ChirpStack components are storing all non-persistent data into a Redis datastore. Redis is used by ChirpStack Network Server and ChirpStack Application Server.
Once the pre requirements are met, the following components form the main constituents of a LORA network:
1. ***Gateway Server Bridge***: ChirpStack Gateway Bridge is a service which converts LoRa Packet Forwarder protocols into a ChirpStack Network Server common data-format (JSON and Protobuf).  This component is part of the ChirpStack open-source LoRaWAN Network Server stack.
2. ***Network Server***: The responsibility of the Network Server component is the de-duplication of received LoRaWAN frames by the LoRa® gateways and for the collected frames handle the:
	Authentication
	LoRaWAN mac-layer (and mac-commands)
	Communication with the ChirpStack Application Server
	Scheduling of downlink frames 
3. ***Application Server***: Responsible for handling the application payloads. The following    additional features are provided:
* Payload encryption / decryption
* Web-interface						   
* User authorization
* API
* Payloads and device events
	* Gateway discovery
	*Live frame-logging and Live Event-logging 

Details of each of these can be found at https://www.chirpstack.io/application-server/overview/features

# Node-Red

**What is Node red?**

Node-RED is a flow-based development tool for visual programming. Node-RED provides a web browser-based flow editor, which can be used to create JavaScript functions. Elements of applications can be saved or shared for re-use. The runtime is built on Node.js. The flows created in Node-RED are stored using JSON.

**Why Node-RED?**

Best developing tool to work with IOT based applications. The programming is simple yet detailed. The interface is easy-to-use even for a layman with no coding knowledge. Node red is best suited for our project because we intend to reach a diverse audience through LORA setup and with Node-Red in place, the end user just needs to drag and drop the node we created and rest will be done automatically by the backend system we have build.

**How the system works?**

 The Node Red is mainky two components - Javascript file and HTML file. 
1.	***Javascript File***: Defines what the node does. The node overall is wrapped as a Node.js module. The module exports a function that gets called when the runtime loads the node on start-up. In the file, a component is added to provide access to Node-Red API at runtime.
2.	***HTML File***: Defines node propeerties and other HTML related components which are desired by the application. We use the basic HTML knowledge to design the desired properties. Combination of HTML and Javascript is done through a package.json file which forms an npm module to be recognized by Node-Red. Further details using an example node can be seen at https://nodered.org/docs/creating-nodes/first-node
# Flask Server
Flask Server is a lightweight web framework specialized for python which supports development of web applications and web resources. The addition of flask server also provides an additional level of security. For this project, we had a ready code("run.py" found in the repository) which starts a flask server which subscribes to all the topics of existing applications at LORA server.

