# MEC-LORA

## Read me

Project Title: **Leveraging RED-Node to support LORA-based IOT applications**


Tutorial Video: **https://drive.google.com/file/d/1XsYWcrjB4LRGjGVMQOqhx-NyEOgJz9Pz/view?usp=sharing**
Supervisor: 
- Adlen Ksentini

Students:
 - SHASTRY sanjeev prasad

This project is a continuation of an old project started by other students at EURECOM.
An appendix file is also given to define the use of tech we worked on.

### Project Definition
>Node-RED (https://nodered.org/) is a programming tool for wiring together hardware devices, APIs and online services in new and interesting ways. It provides a browser-based editor that makes it easy to wire together flows using the wide range of nodes in the palette that can be deployed to its runtime in a single-click. Node-RED is highly employed to design and deploy IOT applications using a web interface, with 0 line of code to write.
Meanwhile, most of the recent IOT deployment is using the LORA family protocols, as the latter enables to running over a low energy system that maximizes the IOT device lifetime. EURECOM has developed its own LORA network using a local Edge Computing platforms, which allows to deploy IOT applications at the edge and connected to LORA network. The expected work during the project is to leverage the RED-Node framework to enable the development and deployment of IOT applications compliant to EURECOM’s LORA Edge platform.

>Key words: IOT, LORA, REST API, Python, RED-Node

### Tech
This project uses a number of open source projects to work properly:
* [Mosquitto] - message broker
* [Flask Server] - lightweight WSGI web application framework
* [Chirpstack] - open-source LoRaWAN Network Server stack
* [Redis] - in-memory data structure 
* [Node-RED] - low-based development tool for visual programming developed

### Installation

##### Mosquitto
Mosquitto installation steps is expanded in [mosquitto_installation].
###### Prerequisites
- An Ubuntu 16.04 server with root access
- Open port TCP:1883 on firewall
###### Commands
#
```sh
$ sudo apt-get update
$ sudo apt-get install mosquitto
$ sudo apt-get install mosquitto-clients
```

##### Redis Server
Redis installation steps is expanded in [redis_installation].
###### Commands
#
```sh
$ wget http://download.redis.io/redis-stable.tar.gz
$ tar xvzf redis-stable.tar.gz
$ cd redis-stable
$ make
```
##### Chirpstack
Chirpstack installation steps is expanded in the following youtube video [chirpstack_installation].
The project providers updated their project from lora to chirpstack so the installation files (name modifications) could be found in their official website found in the upper section but the project could also work with the previous version of Lora so far.
This helpful youtube video could be used: https://www.youtube.com/watch?v=Q0-7zG5uff8&feature=youtu.be
##### Node-RED
Node-RED installation steps is expanded in [nod_installation].


**What is Node-red?**
Node-Red is a flow-based visual programming language which is better suited for ease-of-use applications. Node-red works on a simple principle of designing a '.JS' file which includes the logic for the application and an 'HTML' file that defines how this logic is represented on the portal and later combining the two into a single '.json' package file to add to the Node-Red database.

**Why Node-RED?**
Let us look at our problem statement. We need to primarily design an interface which is easily understandable to the end-user to register devices into the LORA server. The programming in Node-Red is simple yet detailed. The interface is tailor-made even for a layman with no coding knowledge. Node-red is best suited for our project because we intend to reach a diverse audience through LORA setup and with Node-Red in place, the end-user just needs to drag and drop the node we created and rest will be done automatically by the backend system we have built. Through the below explanations, we look in-depth on to how our design is implemented.

###### Prerequisites
- Node.js
###### Commands
# 
With NPM:
```sh
$ sudo npm install -g --unsafe-perm node-red
```
With docker:
```sh
$ docker run -it -p 1880:1880 --name mynodered nodered/node-red
```
With snap:
```sh
$ sudo snap install node-red
```

For this project, the files wihtin the node-red folder should be downloaded to the specific path on the machine in a new folder (ex: test_node): 
```sh
~/.node-red/node_modules/
```
After each modifitcation of these files, you have to install the modifications again to the node-red by running the following command:
```sh
$ npm install
```
**How does the system work**

 The Node-Red is mainly two components - Javascript file and HTML file. 
1.    ***Javascript File***: Fundamentally defines what the node does. The node overall is wrapped as a Node.js module. The module exports a function that gets called when the runtime loads the node on start-up. In the file, a component is added to provide access to Node-Red API at runtime. 
In our case, we have defined a function - 'create_yaml' which contains our logic. The function is called with a single argument, 'RED' that provides the module access to the Node-RED runtime API. We then call on the createNode characteristic of Node-Red which brings in key features shared by all nodes. node.on 'input' in the code indicates that the program will run at each input. We have two different types of selection available for authentication which we leave it to the end-users choice - OTAA(Over The Air Authentication) and ABP(Activation by personalization). The major difference is that when OTAA is invoked, we take in both Application Identifier (AppEUI) and Application Key (AppKey) but the case of ABP asks only for Application Session Key (AppSKey) and the Network Session Key. OTTA performs an additional step of handshake which provides better security. We use javascript knowledge to hide ABP options when OTAA is selected and hide OTAA options when ABP is selected, for user convenience. 

2.    ***HTML File***:  Defines node properties and other HTML related components which are desired by the application. We use the basic HTML knowledge to design the desired properties.  We have to design a simple and yet efficient display which supports layman use. The HTML code attached is self-explanatory and accomplishes this task. 

**The set up in NODE-RED is as displayed in the diagram below. The design logic is implemented in three different stages:**
1. The Application flow
2. The HTTP Flow
3. The Flask server- Yaml interface
#### THE APPLICATION FLOW

Node 1: Input node:
This node is taken directly from the pallete available. Its just used to inject a new input into the flow.

Node 2: Our custom Node:
This node needs user inputs. Read the placeholders in each field carefully before entering the values of your device. Folow is a picture of how the node looks like

![Node](Node_pic.png)

The next section shows the critical config part of the node. As you can see, we have 3 fields to fill and each of this requires special attention as visible in the placeholder instructions as seen blow:

![Node_config](Config_node.png)

Node 3: Take a simple function node from the pallete and write into it the following code:
```sh
$ var newmsg={payload:"localhost:5000/Newtest?query=".concat(msg.payload)}
$ return newmsg;
```
Node 4: Set msg url:
This node is just a continuance to the output of the earlier node and the properties are set as follows:

![set_msg](set_msg.png)

Node 5: Select from the nodes pallete on the left, a 'HTTP Request' node. No further actions needed except connecting it. The node looks like this when double-clicked:

![HTTP_req](HTTP_req.png)

Node 6: Just an output debug node selected from the pallete.

To summarize, here is how ll the connected nodes look like:

![Flow1](Flow1.png)

The Flow starts with an input node which is connected to our custom Test Node. As the code is written to act upon each input, once an input enters the system, we send it to the Node we created. In the custom node, we take in the application data (details of fields found in the javascript explanation) and these pass this data as parameters in message payload to a create URL which now has localhost as the address with msg.payload attached at the end. Then we use the inbuilt Node-Red **Change** Node to set URL according to the payload received. Then this whole detailed setup is represented as an HTTP request and the message payload is available to the user for verification.

### The MQTT Flow

We have a second flow to obtain data back from the lora server - MQTT flow. Here is how th eflow looks like: 

![Flow2](Flow2.png)

Currently we have made use of two LoRa MQTT integration features: Rx and Status messages. 
Each of this node requires a command given below to be inserted into the box as shown in the picture after the command:

```sh
$ application/[application id]/device/[device_eui]/rx[or status depending on the node]
```

![MQTT](mqtt.png)

The MQTT peaks into two integration offerred by chirpstack - Uplink and Status to obtain the corresponding device data. The devie EUI and application id, for now, has to be manually added by the user to the node

### The Flask server Yaml Interface


The final flow of our application is better explained in the form of steps:
    1. In the YAML file, change the "GET" address to our implementation.
    2. We define a function(In this case "nodered_test") which has a query code to extract the device EUI from the localhost. This is stored as "idd".
    3. The "idd" obtained is the key to be passed on to the modification of the YAML process.
    4. Once a connection is established between the two through device EUI, we have an acknowledgement message "Hi You" to verify this to us.
    5. On message reception, MQTT will publish this back to the user so that end-user is fully aware of what data is being stored. This information should also be visible on the LORA server platform.


### Running the project

Start Lora components:

```sh
$ redis-server
```
```sh
$ sudo systemctl start chirpstack-network-server
$ sudo journalctl -f -n 100 -u chirpstack-network-server
```
```sh
$ sudo systemctl start chirpstack-application-server
$ sudo journalctl -f -n 100 -u chirpstack-application-server
```	
```sh
$ sudo systemctl start chirpstack-gateway-bridge
$ sudo journalctl -f -n 100 -u chirpstack-gateway-bridge
```	
Open the following link in the browser:
```sh
http://127.0.0.1:8080/
```
Login with the following default admin credentials:
username: admin
password: admin
Do all the configuration steps presented in the LORA report in order to configure the gateway, devices...

**Start Node-Red:**

```sh
$ node-red
```
Open the following link on a browser:
```sh
http://127.0.0.1:1880/
```

**Start Mosquitto:**
```sh
$ mosquitto -p 2550
$ mosquitto -p 2551
```

**Start Flask server:**
```sh
$ python3 run.py
```
The above file can be found in mec-lora-api-master folder

### Running the project

Having done all the previous steps, node-red and lora server are up.
In Node red:
- Double click on test_node_lora
- Configure the parameters (Be careful to read placeholders)
- Click on deploy button
- Click on deploy node
- You will receive an id in the debugger window.
- Put the application id from chirpstack server and device eui is available from the debugger window. Replace the values in MQTT in nodes

Test:
- Go to Chirpstack server, devices you will find the new deployed devices.
- Run code on the device to send test data and you will see the result on MQTT node red

License
----

EURECOM

[mosquitto_installation]: <https://www.vultr.com/docs/how-to-install-mosquitto-mqtt-broker-server-on-ubuntu-16-04/>
[redis_installation]: <https://redis.io/topics/quickstart>
[chirpstack_installation]: <https://www.youtube.com/watch?v=FnTP7t47DlI>
[Mosquitto]: <https://mosquitto.org/>
[Flask Server]: <https://flask.palletsprojects.com/en/1.1.x/quickstart/>
[Chirpstack]: <https://www.chirpstack.io/>
[Redis]: <https://redis.io/>
[Node-Red]: <https://nodered.org/>

